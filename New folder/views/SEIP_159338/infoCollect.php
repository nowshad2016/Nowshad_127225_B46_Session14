<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <style>
        form{
            max-width:300px;
            padding: 20px;
            margin: auto;
            background-color: #ddd;
        }
        input[type="text"]{
            width: 100%;
            border:0;
            padding: 5px 0;
            margin-bottom: 5px;
            outline: 0;
            border-radius: 5px;
        }
        input[type="submit"]{
            background-color: dodgerblue;
            border:none;
            margin: 20px auto;
            border-radius: 5px;
            color: #fff;
            padding: 5px 10px;
        }
    </style>
</head>
<body>
<form action="result.php" method="post">
    <label>Student Name :</label><br>
    <input type="text"  name="studentName" placeholder=" Name"><br>
    <label>S.I.D </label><br>
    <input type="text"  name="sid" placeholder=" Student ID"><br>
    <label>Bangla :</label><br>
    <input type="text" name="bangla" placeholder=" Mark Obtained"><br>
    <label>English :</label><br>
    <input type="text" name="english" placeholder=" Mark Obtained"><br>
    <label>Math :</label><br>
    <input type="text" name="math" placeholder=" Mark Obtained"><br>
    <input type="submit" name="submit">
</form>
<div id="result"></div>
</body>
</html>