<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 1/21/2017
 * Time: 2:05 PM
 */

namespace App;


class Course
{
    private $bangla;
    private $english;
    private $math;
    private $banglagrade;
    private $englishgrade;
    private $mathgrade;

    public function setBangla($bangla)
    {
        $this->bangla = $bangla;
    }
    public function getBangla()
    {
        return $this->bangla;
    }

    public function setEnglish($english)
    {
        $this->english = $english;
    }
    public function getEnglish()
    {
        return $this->english;
    }

    public function setMath($math)
    {
        $this->math = $math;
    }
    public function getMath()
    {
        return $this->math;
    }

    public function setBanglagrade()
    {
        if($this->bangla >= 80){
            return "A+";
        }else if($this->bangla <= 80 && $this->bangla >= 70){
            return $this->banglagrade = "A";
        }else if($this->bangla <= 70 && $this->bangla >= 60){
            return $this->banglagrade = "A-";
        }else if($this->bangla <= 60 && $this->bangla >= 50){
            return $this->banglagrade = "B";
        }else if($this->bangla <= 50 && $this->bangla >= 40){
            return $this->banglagrade = "C";
        }else if($this->bangla <= 40 && $this->bangla >= 30){
            return $this->banglagrade = "D";
        }else{
            return $this->banglagrade ="F";
        }
    }

    public function getBanglagrade()
    {
      return  $this->setBanglagrade();
    }

    public function setMathgrade()
    {
        if($this->math >= 80){
            return $this->mathgrade = "A+";
        }elseif($this->math <= 80 && $this->math >= 70){
            return $this->mathgrade = "A";
        }elseif($this->math <= 70 && $this->math >= 60){
            return $this->mathgrade = "A-";
        }elseif($this->math <= 60 && $this->math >= 50){
            return $this->mathgrade = "B";
        }elseif($this->math <= 50 && $this->math >= 40){
            return $this->mathgrade = "C";
        }elseif($this->math <= 40 && $this->math >= 30){
            return $this->mathgrade = "D";
        }else{
            return $this->mathgrade ="F";
        }
    }

    public function getMathgrade()
    {
        return $this->setMathgrade();
    }

    public function setEnglishgrade()
    {
        if($this->english >= 80){
            return $this->englishgrade = "A+";
        }elseif($this->english <= 80 && $this->english >= 70){
            return $this->englishgrade = "A";
        }elseif($this->english <= 70 && $this->english >= 60){
            return $this->englishgrade = "A-";
        }elseif($this->english <= 60 && $this->english >= 50){
            return $this->englishgrade = "B";
        }elseif($this->english <= 50 && $this->english >= 40){
            return $this->englishgrade = "C";
        }elseif($this->english <= 40 && $this->english >= 30){
            return $this->englishgrade = "D";
        }else{
            return $this->englishgrade ="F";
        }
    }

    public function getEnglishgrade()
    {
        return $this->setEnglishgrade();
    }

}